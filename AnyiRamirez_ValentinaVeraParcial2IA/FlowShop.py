#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import array
import random
import json
import time

import numpy

from deap import algorithms
from deap import base
from deap import creator
from deap import tools

# gr*.json contains the time of tasks in list of list style in JSON format
with open("gr17.json", "r") as tsp_data:
    tsp = json.load(tsp_data)

TimeMatrix = tsp["TimeMatrix"]
IND_SIZE = tsp["numberTask"]

creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", array.array, typecode='i', fitness=creator.FitnessMin)

toolbox = base.Toolbox()

# Attribute generator
toolbox.register("indices", random.sample, range(IND_SIZE), IND_SIZE)

# Structure initializers
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

def getlistTask(individual, listTask):
    varTask = 0
    for i in range(len(individual)):
        varPos = individual[i]
        varTask = varTask + TimeMatrix[0][varPos]
        listTask.append(varTask)

def getListMachine(individual, listMachine):
    varMac = 0
    for i in range(len(TimeMatrix)):
        varPos = individual[0]
        varMac = varMac + TimeMatrix[i][varPos]
        listMachine.append(varMac)
    

def evalOrderTasks(individual):
    listTask = []
    getlistTask(individual, listTask)
    listMachine = []
    getListMachine(individual, listMachine)
    i = 1
    while i in range(len(listMachine)):
        subListTask = []
        varUp = listMachine[i]
        subListTask.append(varUp)
        j = 1
        while j in range(len(listTask)):
            varSide = listTask[j]
            maximo = max(varUp,varSide)
            timer = TimeMatrix[i][individual[j]]
            newUp = timer+maximo
            subListTask.append(newUp)
            varUp = newUp
            
            j+=1
        listTask = subListTask
        i+=1
        
    return listTask[len(listTask)-1],
    
toolbox.register("mate", tools.cxPartialyMatched)
toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.15)
toolbox.register("select", tools.selTournament, tournsize=5)
toolbox.register("evaluate", evalOrderTasks)

def main():
    random.seed(time.time())

    pop = toolbox.population(n=100)

    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)
    
    algorithms.eaSimple(pop, toolbox, 0.7, 0.2, 50, stats=stats, 
                        halloffame=hof)
    print(hof)
    
    return pop, stats, hof

if __name__ == "__main__":
    main()
