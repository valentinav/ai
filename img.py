import skimage.io as io
from skimage.color import rgb2gray
from sklearn.decomposition import PCA
import numpy as np
import os
from os.path import isfile, join
#ESTAMOS CREADNO EL ARCHIVO DE ENTRENAMIENTO.
path='./DataSetDigitos/mnist_png/training/' #RUTA DE DONDE ESTAN LOS DATOS DE ENTRENAMIENTO --> PARA GENERAR EL ARCHIVO DE TESTING CAMBIAR TRAINING POR TESTING.
                                            #--> EL ARCHIVO DE TESTING SE LA PASO A LA RED NEURONAL , CON PREDIG NOS MUESTRA LAS ESTADISTICAS, GRAFICA DEL ERROR.
                                            #-->LA GRAFIA DEL ENTRENAMIENTO, GRAFICA DE VALIDACION TENGAN EL MISMO COMPRTAMIENTO, SINO HAY SOBRE ENTRENAMIENTO / SOBRE APRENDIZAJE.

#-->> EN PRODUCCION:

'''
imgs = io.imread_collection(4/*.png')
'''

listdir=os.listdir(path) #METODO LIBRERIA OS, SACA TODAS LAS CARPETAS QUE ESTAN EN EL PATH (0,1,2,3...9)
cleandir=[] #VECHAR DE PATHS DE DIRECTORIOS
for filename in listdir:
    if not isfile(join(path,filename)): #ARMA EL PATH PARA ENTRAR A LAS SUBCARPETAS
        cleandir.append(filename)


training=open('training.txt','w') #CREAR EL ARCHIVO DONDE GUARDAR LOS DATOS DEL VECTOR DE COMPONENTES --> PARA GENERAR EL ARCHIVO DE TESTING CAMBIAR TRAINING POR TESTING

for directory in cleandir: #PARA CADA UNO DE LOS PATHS SACAN LA SALIDA DESEADA
    desired=bin(int(directory))[2:].zfill(4) #COMO EL NOMBRE DE LA CARPETA ES UN NUMERO LO CONVIERTE EN UN ENTERO Y LUEGO EN BINARIO DE 4 POSICIONES
    
    pdirectory=path+directory+'/*.png'  #DENTRO DEL DIRECTORIO BUSCA TODOS LOS ARCHIVOS QUE SEAN .PNG
    imgs = io.imread_collection(pdirectory) #LEER TODAS LAS IMAGENES QUE ESTAN DENTRO DE ESE DIRECTORIO
    for img in imgs: #PARA CADA UNA DE LAS IMAGENES QUE ENCONTRO DENTRO DE ESE DIRECTORIO
        imgray = rgb2gray(img) #PASA LA IMAGEN A ESCALA DE GRISES
        mcolor = np.array(imgray)/255 #ARMA LA MATRIZ DE VALORES CON EL COLOR EN ESCALA DE GRISES QUE HAY EN CADA PIXEL DIVIDE PARA NORMALIZAR DAN VALORES ENTRE 0 Y 1.
        vcolor=mcolor.ravel() #------------¿PARA QUE ES ?--------------
        '''
        io.imshow(imgray) #MOSTRAR CADA IMAGEN.
        io.show()
        '''
 
        pca = PCA(n_components=1) #OBJ DE TIPO PCA , 1 ES LA DIAGONAL PRINCIPAL
        pca.fit(mcolor) #PASAMOS EL ARREGLO YA NORMALIZADO
        V = pca.components_ #SACA EL COMPONENTE
        V=V.flatten() #PONE EN FORMATO DE VECTOR (FORMATO PLANO)
        for value in V: #PARA CADA UNO DE LOS VECTORES LOS ESCRIBO EN EL ARVHIVO
            training.write(str(value)+'\t')
        for value in desired:#PARA CADA UNO DE LOS VECTORES ESCRIBO AL FRENTE LA SALIDA DESEADA
            training.write(str(value)+'\t') 
            
        training.write('\n')
        
training.close()

	

        

