#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import array
import random
import math
import time

import numpy as np

from deap import algorithms
from deap import base
from deap import creator
from deap import tools

sections = 3
decimales = 5
genLeng =   1  +  sections  +   decimales
numGen = 8

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", array.array, typecode='b', fitness=creator.FitnessMax)

toolbox = base.Toolbox()

# Attribute generator
toolbox.register("attr_bool", random.randint, 0, 1)


def evaluate(individual):
    valores = []
    for i in range(numGen):
        limite = i+genLeng
        signo = individual[i]
        n = sections-1
        value = 0
        for bit in individual[i+1:limite]:
            value += bit*math.pow(2,n)
            n-=1
        if  signo == 0:
            value = -value
        valores.append(value)
    return valores

def evalOneMax(x):
    #FUNCIÓN DE ACKLEY
    a = 20
    b = 0.2
    c = 2*(math.pi)
    d = len(x)
    sumaX2= sum([(xi*xi) for xi in x])
    sumCosX= sum([math.cos(c*xi) for xi in x])
    r = -a * math.exp(-b*math.sqrt(sumaX2/d)) - math.exp(sumCosX/d) + a + math.exp(1)
    return r,

def configureAlgorithm(toolbox, individualLen,funcion_eval):
    # Structure initializers
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_bool, individualLen)#4 es el tamaño del individuo
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("evaluate", funcion_eval)


#toolbox.register("evaluate", evalOneMax)
toolbox.register("mate", tools.cxOnePoint)#operador de cruce en un solo punto
toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)#operador de mutación, indpb la probabilidad de mutación
toolbox.register("select", tools.selTournament, tournsize=3) #torneo = quien tenga mejor función de evaluación es el seleccionado, participan 3 individuos

def main():
    configureAlgorithm(toolbox,genLeng*numGen,lambda x: evalOneMax(evaluate(x)))
    random.seed(time.time()) #semilla para generador de numeros aleatorios
    
    pop = toolbox.population(n=20)#define el tamaño de la población
    hof = tools.HallOfFame(1)#cuantos de los mejores de una generación, pasan a la siguiente, se escoge 1 (Elitismo)

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)#imprime estadistias de lo que pasa durante la ejecución
    
    pop, log = algorithms.eaSimple(pop, toolbox, cxpb=0.5, mutpb=0.2, ngen=40, 
                                   stats=stats, halloffame=hof, verbose=True)#algoritmo genético

    print(hof)#muestra el mejor individuo
    """for hofi in hof:
        value =  evaluate(hofi)
        print(value, "fintess: ",evalOneMax(value)[0])
        pass"""
    return pop, log, hof


if __name__ == "__main__":
    main()
