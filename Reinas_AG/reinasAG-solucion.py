#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import array
import random
import json
import time

import numpy

from deap import algorithms
from deap import base
from deap import creator
from deap import tools

IND_SIZE = 8

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", array.array, typecode='i', fitness=creator.FitnessMax)

toolbox = base.Toolbox()

# Attribute generator
toolbox.register("indices", random.sample, range(IND_SIZE), IND_SIZE)

# Structure initializers
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

def evalTablero(individual):
    evaluacion = 0
    for i in range(len(individual)):
        reina = individual[i] #numero que está en esa posicion de la cadena
        j = i+1
        #recorrido Superior
        while j in range(len(individual)):
            pasoDiagonalSup = j-i
            reinaSup = reina - pasoDiagonalSup #posible par de reina en la diagonal superior
            if (reinaSup<0):
                break
            if (individual[j] == reinaSup):
                break
            else:
                evaluacion+=1
            
            j+=1

        k=i+1
        #recorrido inferior
        while k in range(len(individual)):
            pasoDiagonalInf = k-i
            reinaInf = reina + pasoDiagonalInf #posible par de reina en la diagonal inferior
            if (reinaInf>=IND_SIZE):
                break
            if(individual[k] == reinaInf):
                break
            else:
                evaluacion+=1
            k+=1
    return evaluacion,

toolbox.register("mate", tools.cxPartialyMatched)
toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.15)
toolbox.register("select", tools.selTournament, tournsize=5)
toolbox.register("evaluate", evalTablero)

def main():
    random.seed(time.time())

    pop = toolbox.population(n=100)

    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)
    
    algorithms.eaSimple(pop, toolbox, 0.7, 0.2, 50, stats=stats, 
                        halloffame=hof)
    print(hof)
    
    return pop, stats, hof

if __name__ == "__main__":
    main()
