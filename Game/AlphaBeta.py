class AlphaBeta:

    def __init__(self,game):
        self.game = game 
        
    def max_value(self, state, alpha, beta):
        if self.game.terminal_test(state):
            return self.game.utility(state, player)
        v = -infinity
        for a in self.game.actions(state):
            v = max(v, min_value(self.game.result(state, a), alpha, beta))
            if v >= beta:
                return v
            alpha = max(alpha, v)
        return v

    def min_value(self, state, alpha, beta):
        if self.game.terminal_test(state):
            return self.game.utility(state, player)
        v = infinity
        for a in self.game.actions(state):
            v = min(v, max_value(self.game.result(state, a), alpha, beta))
            if v <= alpha:
                return v
            beta = min(beta, v)
        return v
    
    def run (state):
        player = self.game.to_move(state)
        best_score = -infinity
        beta = infinity
        best_action = None
        for a in self.game.actions(state):
            v = min_value(self.game.result(state, a), best_score, beta)
            if v > best_score:
                best_score = v
                best_action = a
        return best_action
    

    







