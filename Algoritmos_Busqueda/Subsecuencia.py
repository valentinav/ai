from Node import Node
from Problem import Problem

class Subsecuencia(Problem):

    
    def __init__(self,lista, final_result):
        initial_state = []
        self.l = lista
        self.result = final_result
        super().__init__(initial_state,final_state=None)

    def child_node(self,node):
        child=[]
        for i in range(len(self.l)):
            son=self.Insert(node,i)
            if(son!=None):
                child.append(son)

        return child
    def getNewList(self,aux,elemento):
        newList = []
        for i in range(len(aux)):
            newList.insert(len(newList),aux[i])
        newList.append(elemento)
        return newList

    def areDifferent(self, state, parent, i):
        long = len(state)        
        for st in range(long):
            if i<=st:
                if state[i]== self.l[i]:
                    return False
            if i>st:
                if state[st]==self.l[i]:
                    return False
            if i == st:
                if state[len(state)-1] == self.l[i]:
                    return False

        return True

    def Insert(self,node,i):
        state = node.State
        if(len(state)==0):
            newList = self.getNewList(state, self.l[i])
            new_state=(newList)
            
            new_node= Node(new_state,parent=node,action='insert_')
            return new_node
        parent = node.parent
        if self.areDifferent(state, parent, i):
            newList = self.getNewList(state, self.l[i])
            new_state=(newList)
            new_node= Node(new_state,parent=node,action='insert_')
            
            return new_node
            
        else:
            return None
        

    def goal_test(self,node):
        if isinstance(node, Node):
            aux = node.State
            suma = 0
            for i in range(len(aux)):
                suma = suma + aux[i]
                
            if suma == self.result:
                return True
            else:
                return False
        else:
            return False

            
