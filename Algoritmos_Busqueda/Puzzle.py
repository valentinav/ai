import math

from Node import Node
from Problem import Problem

class Puzzle(Problem):
    def __init__(self, initial_state, final_state):
        self.size = self.getSize_Puzzle(initial_state)
        super().__init__(initial_state, final_state)

    def child_node(self,node):
        child = []

        son = self.move_up(node)
        if(son!=None):
            child.append(son)

        son = self.move_down(node)
        if(son!=None):
            child.append(son)
            
        son = self.move_left(node)
        if(son!=None):
            child.append(son)
            
        son = self.move_right(node)
        if(son!=None):
            child.append(son)

        return child

    def ManhattanFormula(self,Nx,Ny,Gx,Gy):
        """ h(n)= |n.x - goal.x| + |n.y - goal.y| """
        valAbsx = abs(Nx-Gx)
        valAbsy = abs(Ny-Gy)
        Hn = valAbsx + valAbsy
        return Hn

    def ManhattanDistance(self, node):
        state = node.State
        goalstate= (self.Goal_Node).State
        summationMD = 0
        for i in range(len(state)):
            pos = self.getPos (state,state[i])
            posGoal = self.getPos(goalstate,state[i])
            summationMD += self.ManhattanFormula(pos[0],pos[1],posGoal[0],posGoal[1])
        return summationMD

    def getPos(self,l,element):
        f=[]
        x=0
        while x in range(self.size):
            f.append(l[:self.size])
            n=f[x]
            for y in range(len(n)):
                if n[y] == element:
                    t=(x,y)
                    return t
            l=l[self.size:]
            x+=1      

    def getSize_Puzzle(self,node):
        return int(math.sqrt(len(node)))

    def find_Zero(self,state):
        for i in range(len(state)):
            if state[i]==0:
                return i

    def new_tuple(self,state,new_pos,pos_0):
        l=[]
        for i in range(len(state)):
            l.append(state[i])
        l[pos_0]=state[new_pos]
        l[new_pos]=0
        t = tuple(l)
        return t           

    def move_up(self, node):
        state = node.State
        pos_0 = self.find_Zero(state)
        up = pos_0 + self.size
        if up < len(state):
            varTuple = self.new_tuple(state, up, pos_0)
            new_state = (varTuple)
            new_node= Node(new_state,parent=node,action='move_up')
            return new_node
        else:
            return None
        
    def move_down (self, node):
        state = node.State
        pos_0 = self.find_Zero(state)
        down = pos_0 - self.size
        if down >= 0:
            varTuple = self.new_tuple(state, down, pos_0)
            new_state = (varTuple)
            new_node= Node(new_state,parent=node,action='move_down')
            return new_node
        else:
            return None
        
    def move_left(self, node):
        state = node.State
        pos_0 = self.find_Zero(state)
        if self.can_moveL(pos_0,state):
            varTuple = self.new_tuple(state,(pos_0+1), pos_0)
            new_state = (varTuple)
            new_node= Node(new_state,parent=node,action='move_left')
            return new_node
        else:
            return None
  
    def can_moveL(self,pos_0,state):
        var = False
        num = len(state)
        if (pos_0+1)==num:
            return var
        for i in range(self.size):
            if (pos_0+1)== (num-self.size):
                return var
            else:
                num = num-self.size
        var = True
        return var

    def move_right(self, node):
        state = node.State
        pos_0 = self.find_Zero(state)
        if self.can_moveR(pos_0,state):
            varTuple = self.new_tuple(state,(pos_0-1), pos_0)
            new_state = (varTuple)
            new_node= Node(new_state,parent=node,action='move_right')
            return new_node
        else:
            return None
            
    def can_moveR(self,pos_0,state):
        var = False
        num = len(state)-1
        if pos_0==0:
            return var
        
        for i in range(self.size):
            if num<0:
                return var
            if (pos_0-1)== (num-self.size):
                return var
            else:
                num = num-self.size
        var = True
        return var
    
    
