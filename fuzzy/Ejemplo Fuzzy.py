#pip install -U scikit-fuzzy
#https://pythonhosted.org/scikit-fuzzy/api/skfuzzy.membership.html

import numpy as np
import skfuzzy as fuzz #buscar los operadores (min, max, etc...)
from skfuzzy import control as ctrl

quality = ctrl.Antecedent(np.arange(0, 11, 1), 'quality')#se evalua de 1 a (11-1) y de la 1(1,2,3,4,...).
service = ctrl.Antecedent(np.arange(0, 11, 1), 'service')
tip = ctrl.Consequent(np.arange(0, 13, 1), 'tip')
#antecedente es entradas y consecuente es salidas

quality['low']=fuzz.trimf(quality.universe,[0, 0, 5])
quality['medium']=fuzz.trapmf(quality.universe,[1, 3, 7,9])
quality['high']=fuzz.trimf(quality.universe,[5, 10, 10])

service['low']=fuzz.trimf(service.universe,[0, 0, 4])
service['medium']=fuzz.trimf(service.universe,[2, 4, 6])
service['high']=fuzz.trimf(service.universe,[4, 6, 8])
service['veryhigh']=fuzz.trimf(service.universe,[6, 10, 10])


tip['low'] = fuzz.trimf(tip.universe, [0, 0, 12])
tip['medium'] = fuzz.trimf(tip.universe, [0, 6, 12])
tip['high'] = fuzz.trimf(tip.universe, [0, 12, 12])


#quality['average'].view()

#service.view()

#tip.view()


rule1 = ctrl.Rule(quality['low'] | service['low'], tip['low'])
rule2 = ctrl.Rule(quality['low'] & service['medium'], tip['low'])
rule3 = ctrl.Rule(quality['low'] & service['high'], tip['low'])
rule4 = ctrl.Rule(quality['low'] & service['veryhigh'], tip['medium'])

rule5 = ctrl.Rule(quality['medium'] & service['low'], tip['low'])
rule6 = ctrl.Rule(quality['medium'] & service['medium'], tip['low'])
rule7 = ctrl.Rule(quality['medium'] & service['high'], tip['medium'])
rule8 = ctrl.Rule(quality['medium'] & service['veryhigh'], tip['high'])

rule9 = ctrl.Rule(quality['high'] & service['low'], tip['medium'])
rule10 = ctrl.Rule(quality['high'] & service['medium'], tip['medium'])
rule11 = ctrl.Rule(quality['high'] & service['high'], tip['high'])
rule12 = ctrl.Rule(quality['high'] & service['veryhigh'], tip['high'])


#rule1.view()



tipping_ctrl = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5,
                                   rule6, rule7, rule8, rule9, rule10, rule11, rule12])


tipping = ctrl.ControlSystemSimulation(tipping_ctrl)

#se evalua el sistema con funciones de transferencia(se deja constante una y se varía la otra, se grafica)
tipping.input['quality'] = 10.0
tipping.input['service'] = 10.0

tipping.compute()

print (tipping.output['tip'])
#tip.view(sim=tipping)
