import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPRegressor

np.random.seed(3)
n = 1000
x = np.random.uniform(-15, 15, size = n)
y = x**2 + 2*np.random.randn(n, )


m=20
tx = np.random.uniform(-15, 15, size = m)
ty = tx**2 + 2*np.random.randn(m, )

X = np.reshape(x ,[n, 1]) 
Y = np.reshape(y ,[n ,])

TX = np.reshape(tx ,[m, 1])
TY = np.reshape(ty ,[m ,])

print(Y)

clf = MLPRegressor(alpha=0.001, hidden_layer_sizes = (10,), max_iter = 100000, 
                 activation = 'logistic', verbose = 'True', learning_rate = 'adaptive')
a = clf.fit(X, Y)
pred=clf.predict(TX)



