from sklearn.neural_network import MLPClassifier
from sklearn.metrics import classification_report,confusion_matrix
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report,confusion_matrix


X = [[0, 0],[1, 0],[0, 1],[1, 1]]
Y = [0,0,0,1]

XP = [[0, 0],[1, 0],[0, 1],[1, 1]]


clf = MLPClassifier(solver='sgd', activation='relu', alpha=1e-9,tol=1e-12,
                    hidden_layer_sizes=(3,),learning_rate='constant',
                    learning_rate_init=0.01, max_iter=1000, momentum=0.9, verbose=True)#hidden layer es 3 lo que quiere decir que a la red hay que definirle
                                                                                        # las neuronas del medio, la taza de aprendizaje y la de aprendizaje
                                                                                        #son dos parametros del algoritmo de entrenamiento y el parametro momentum
                                                                                        #también le sirve para salir. el argoritmo también funciona por iteraciones max_iter


'''
solver : {‘lbfgs’, ‘sgd’, ‘adam’}, default ‘adam’

The solver for weight optimization.

‘lbfgs’ is an optimizer in the family of quasi-Newton methods.
‘sgd’ refers to stochastic gradient descent.
‘adam’ refers to a stochastic gradient-based optimizer proposed by Kingma, Diederik, and Jimmy Ba
'''
'''
activation : {‘identity’, ‘logistic’, ‘tanh’, ‘relu’}, default ‘relu’

Activation function for the hidden layer.

‘identity’, no-op activation, useful to implement linear bottleneck, returns f(x) = x
‘logistic’, the logistic sigmoid function, returns f(x) = 1 / (1 + exp(-x)).
‘tanh’, the hyperbolic tan function, returns f(x) = tanh(x).
‘relu’, the rectified linear unit function, returns f(x) = max(0, x)

'''
#cada iteración va a tener un error y debe tender a 0 con el tiempo
#con esto se entrena
clf.fit(X, Y)
#se pone a funcionar, prediciendo con ejemplos de prueba
predict=clf.predict(XP)

print(confusion_matrix(Y,predict))
print(classification_report(Y,predict))

print(predict)

loss_values = clf.loss_curve_ #obtiene los errores de cada iteración
plt.plot(loss_values)
plt.show()







