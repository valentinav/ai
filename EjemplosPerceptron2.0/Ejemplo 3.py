from sklearn.neural_network import MLPRegressor
from sklearn.metrics import classification_report,confusion_matrix
import matplotlib.pyplot as plt
import numpy as np

x=[]
y=[]
test=[[0.91,0.86]]

dataset = np.loadtxt('Imc1.txt', delimiter=",")

for pattern in dataset:
    x.append(pattern[:2])#colunmas de entradas
    y.append(pattern[2:])#colunmas de salida

y=np.ravel(y) #buscar que hace ravel, le hace un preprocesamiento a y

clf = MLPRegressor(alpha=0.01, hidden_layer_sizes = (2,), max_iter = 100,tol=1e-19, learning_rate_init=0.1,
                 momentum=0.9,activation = 'logistic', verbose = 'True', learning_rate = 'adaptive')
#se usa un regresor porque se va a aproximar una función (MLPRegressor), una capa con 2 neuronas 
clf.fit(x, y)
'''
predict=clf.predict(x)
'''


loss_values = clf.loss_curve_
plt.plot(loss_values)
plt.show()

"""
fileout=open("prediccion.txt","w")

for salidas in predict:
    fileout.write(str(salidas)+"\n")

fileout.close()
"""
predict2=clf.predict(test)
print(predict2)



    

