from sklearn.neural_network import MLPClassifier
from sklearn.metrics import classification_report,confusion_matrix
import numpy as np

inputs=8
x=[]
y=[]
dataset = np.loadtxt('Music.txt', delimiter=",")
for pattern in dataset:
    x.append(pattern[:8])
    y.append(pattern[8:])
    


    
clf = MLPClassifier(solver='lbfgs', activation='relu', alpha=1e-5,
                    hidden_layer_sizes=(8, 3),learning_rate='constant',
                    learning_rate_init=0.001, max_iter=100, momentum=0.9)
clf.fit(x, y)

predict=clf.predict(x)
print(predict)

    

