(deftemplate figure
    (slot x (allowed-numbers 0 1 2 3 4 5 6))
    (slot y (allowed-numbers 0 1 2 3 4 5 6))
    (slot type (allowed-values pac-man monster food))
    (slot direction (allowed-values up down left rigth none) (default none))
)
(deftemplate wall
    (slot x1 (allowed-numbers 0 1 2 3 4 5 6 7))
    (slot x2 (allowed-numbers 0 1 2 3 4 5 6 7))
    (slot y1 (allowed-numbers 0 1 2 3 4 5 6 7))
    (slot y2 (allowed-numbers 0 1 2 3 4 5 6 7))
)

;DEFINICION DE REGLAS
(defrule pac-man-wins-game
    (not (figure (type food)))
    (figure (type pac-man) (direction ~none))
    =>
    (printout t "Pac-man wins game")
)
(defrule pac-man-loses-game
    (not (figure (type pac-man)))
    (figure (type monster) (direction ~none))
    =>
    (printout t "Pac-man loses game")
)
(defrule finish-game
    (not (figure (type food)))
    ?figure <-(figure (type ~food) (direction ~none))
    =>
    (modify ?figure (direction none))
)
(defrule pac-man-eats-food
    ?food <- (figure (x ?x) (y ?y) (type food))
    (figure (x ?x) (y ?y) (type ~food))
    =>
    (retract ?food)
    (printout t "Pac-man eats food")
)
(defrule monster-kills-pac-man
    ?pac-man <- (figure (x ?x) (y ?y) (type pac-man))
    (figure (x ?x) (y ?y) (type monster))
    =>
    (retract ?pac-man)
    (printout t "Monster killed Pac-man")
)
(defrule pac-man-can-turn-to-up
    (not (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2)))
    (figure (x ?x) (y ?y) (type pac-man) (direction ~up&~none))
    (and (eq ?y1 ?y) (eq ?y2 ?y))
    (and (<= ?x1 ?x) (> ?x2 ?x))
    =>
    (printout t "Pac-man can move to up")
)
(defrule pac-man-can-turn-to-down
    (not (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2)))
    (figure (x ?x) (y ?y) (type pac-man) (direction ~down&~none))
    (and (eq ?y1 ?y) (eq ?y2 ?y))
    (and (<= ?x1 :(+ 1 ?x)) (> ?x2 :(+ 1 ?x)))
    =>
    (printout t "Pac-man can move to down")
)
(defrule pac-man-can-turn-to-left
    (not (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2)))
    (figure (y ?y) (x ?x) (type pac-man) (direction ~left&~none))
    (and (eq ?x1 ?x) (eq ?x2 ?x))
    (and (<= ?y1 ?y) (> ?y2 ?y))
    =>
    (printout t "Pac-man can move to left")
)
(defrule pac-man-can-turn-to-rigth
    (not (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2)))
    (figure (x ?x) (y ?y) (type pac-man) (direction ~rigth&~none))
    (and (eq ?x1 ?x) (eq ?x2 ?x))
    (and (<= ?y1 :(+ 1 ?y)) (> ?y2 :(+ 1 ?y)))
    =>
    (printout t "Pac-man can move to rigth")
)
(defrule figure-moves-to-up
    (not (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2)))
    ?figure <-(figure (x ?x) (y ?y) (type ~food) (direction up))
    (and (eq ?y1 ?y) (eq ?y2 ?y))
    (and (<= ?x1 ?x) (> ?x2 ?x))
    =>
    (modify ?figure (y (- ?y 1)))
    (printout t "The " ?figure " moves to up")
)
(defrule figure-moves-to-down
    (not (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2)))
    ?figure <-(figure (x ?x) (y ?y) (type ~food) (direction down))
    (and (eq ?y1 ?y) (eq ?y2 ?y))
    (and (<= ?x1 :(+ 1 ?x)) (> ?x2 :(+ 1 ?x)))
    =>
    (modify ?figure (x (+ ?x 1)))
    (printout t " The " ?figure " moves to down")
)
(defrule figure-moves-to-left
    (not (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2)))
    ?figure <-(figure (y ?y) (x ?x) (type ~food) (direction left))
    (and (eq ?x1 ?x) (eq ?x2 ?x))
    (and (<= ?y1 ?y) (> ?y2 ?y))
    =>
    (modify ?figure (y (- ?y 1)))
    (printout t " The " ?figure " moves to left")
)
(defrule figure-moves-to-rigth
    (not (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2)))
    ?figure <-(figure (x ?x) (y ?y) (type ~food) (direction rigth))
    (and (eq ?x1 ?x) (eq ?x2 ?x))
    (and (<= ?y1 :(+ 1 ?y)) (> ?y2 :(+ 1 ?y)))
    =>
    (modify ?figure (y (+ ?y 1)))
    (printout t " The " ?figure " moves to rigth")
)
(defrule change-direction-to-moster
    ?moster <- (figure (type monster) (direction ?direction))
    =>
    (switch (mod (random 0 100) 4)
    (case 0 then (modify ?moster (direction up)))
    (case 1 then (modify ?moster (direction down)))
    (case 2 then (modify ?moster (direction left)))
    (case 3 then (modify ?moster (direction rigth)))
    )
)
;DEFINICION DE HECHOS
(assert (wall (x1 0) (x2 0) (y1 0) (y2 8)))
(assert (wall (x1 8) (x2 8) (y1 0) (y2 8)))
(assert (wall (x1 0) (x2 8) (y1 0) (y2 0)))
(assert (wall (x1 0) (x2 8) (y1 8) (y2 8)))
(assert (wall (x1 1) (x2 3) (y1 2) (y2 2)))
(assert (wall (x1 1) (x2 4) (y1 3) (y2 3)))
(assert (wall (x1 2) (x2 5) (y1 7) (y2 7)))
(assert (wall (x1 3) (x2 3) (y1 1) (y2 4)))
(assert (wall (x1 5) (x2 5) (y1 2) (y2 7)))
(assert (wall (x1 7) (x2 7) (y1 5) (y2 8)))
(assert (figure (x 0) (y 0) (type food)))
(assert (figure (x 1) (y 1) (type food)))
(assert (figure (x 2) (y 2) (type food)))
(assert (figure (x 3) (y 3) (type food)))
(assert (figure (x 4) (y 4) (type food)))
(assert (figure (x 5) (y 5) (type food)))
(assert (figure (x 6) (y 6) (type food)))
(assert (figure (x 7) (y 7) (type food)))
(assert (figure (x 0) (y 0) (type pac-man) (direction down)))
(assert (figure (x 7) (y 7) (type monster) (direction up)))