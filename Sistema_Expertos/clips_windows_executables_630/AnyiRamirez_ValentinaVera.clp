(deftemplate figure
  (slot x (allowed-numbers 0 1 2 3 4 5 6 7))
  (slot y (allowed-numbers 0 1 2 3 4 5 6 7))
  (slot type (allowed-values pac-man monster food))
  (slot direction (allowed-values up down left right none) (default none))
)
(deftemplate wall
  (slot x1 (allowed-numbers 0 1 2 3 4 5 6 7))
  (slot x2 (allowed-numbers 0 1 2 3 4 5 6 7))
  (slot y1 (allowed-numbers 0 1 2 3 4 5 6 7))
  (slot y2 (allowed-numbers 0 1 2 3 4 5 6 7))
)

(defrule pac-man-wins-game
    (not (figure (type food)))
    (figure (type pac-man) (direction ~none))
    =>
    (printout t "Pac-man wins game" crlf)
)
(defrule pac-man-loses-game
    (not (figure (type pac-man)))
    (figure (type monster) (direction ~none))
    =>
    (printout t "Pac-man loses game" crlf)
)
(defrule finish-game
    (not (figure (type ~monster)))
    ?figure <-(figure (type ~food) (direction ~none))
    =>
    (modify ?figure (direction none))
)
(defrule pac-man-eats-food
    ?food <- (figure (x ?x) (y ?y) (type food))
    (figure (x ?x) (y ?y) (type ~food))
    =>
    (retract ?food)
    (printout t "Pac-man eats food" crlf)
)
(defrule monster-kills-pac-man
    ?pac-man <- (figure (x ?x) (y ?y) (type pac-man))
    (figure (x ?x) (y ?y) (type monster))
    =>
    (retract ?pac-man)
    (printout t "Monster killed Pac-man" crlf)
)

(defrule pac-man-moves-to-up
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type pac-man) (direction up))
  (test (> ?y 0))
  (test (eq ?y1 ?y2))
  (test (or (neq ?y1 :(- ?y 1)) (> ?x1 ?x) (< ?x2 ?x)))
  =>
  (modify ?figure (y (- ?y 1)))
  (printout t "Pac-man move to up" crlf)
    (assert (figure (x ?x) (y ?y) (type pac-man) (direction left)))
)
(defrule pac-man-moves-to-down
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type pac-man) (direction down))
  (test (< ?y 7))
  (test (eq ?y1 ?y2))
  (test (or (neq ?y1 :(+ ?y 1)) (> ?x1 ?x) (< ?x2 ?x)))
  =>
  (modify ?figure (y (+ ?y 1)))
  (printout t "Pac-man move to down" crlf)
)
(defrule pac-man-moves-to-left
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type pac-man) (direction left))
  (test (> ?x 0))
  (test (eq ?x1 ?x2))
  (test (or (neq ?x1 :(- ?x 1)) (> ?y1 ?y) (< ?y2 ?y)))
  =>
  (modify ?figure (x (- ?x 1)))
  (printout t "Pac-man move to left" crlf)
)
(defrule pac-man-moves-to-right
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type pac-man) (direction right))
  (test (< ?x 7))
  (test (eq ?x1 ?x2))
  (test (or (neq ?x1 :(+ ?x 1)) (> ?y1 ?y) (< ?y2 ?y)))
  =>
  (modify ?figure (x (+ ?x 1)))
  (printout t "Pac-man move to right" crlf)
  (assert (figure (x ?x) (y ?y) (type pac-man) (direction up)))
)


(defrule monster-moves-to-up
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type monster) (direction up))
  (test (> ?y 0))
  (test (eq ?y1 ?y2))
  (test (or (neq ?y1 :(- ?y 1)) (> ?x1 ?x) (< ?x2 ?x)))
  =>
  (modify ?figure (y (- ?y 1)))
  (printout t "monster move to up" crlf)
    (assert (figure (x ?x) (y ?y) (type monster) (direction left)))
)
(defrule monster-moves-to-down
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type monster) (direction down))
  (test (< ?y 7))
  (test (eq ?y1 ?y2))
  (test (or (neq ?y1 :(+ ?y 1)) (> ?x1 ?x) (< ?x2 ?x)))
  =>
  (modify ?figure (y (+ ?y 1)))
  (printout t "monster move to down" crlf)
)
(defrule monster-moves-to-left
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type monster) (direction left))
  (test (> ?x 0))
  (test (eq ?x1 ?x2))
  (test (or (neq ?x1 :(- ?x 1)) (> ?y1 ?y) (< ?y2 ?y)))
  =>
  (modify ?figure (x (- ?x 1)))
  (printout t "monster move to left" crlf)
    (assert (figure (x ?x) (y ?y) (type monster) (direction down)))
)
(defrule monster-moves-to-right
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type monster) (direction right))
  (test (< ?x 7))
  (test (eq ?x1 ?x2))
  (test (or (neq ?x1 :(+ ?x 1)) (> ?y1 ?y) (< ?y2 ?y)))
  =>
  (modify ?figure (x (+ ?x 1)))
  (printout t "monster move to right" crlf)
)


;DEFINICION DE HECHOS
(assert (wall (x1 0) (x2 0) (y1 0) (y2 7)))
(assert (wall (x1 7) (x2 7) (y1 0) (y2 7)))
(assert (wall (x1 0) (x2 7) (y1 0) (y2 0)))
(assert (wall (x1 0) (x2 7) (y1 7) (y2 7)))

(assert (wall (x1 2) (x2 4) (y1 2) (y2 2)))
(assert (wall (x1 1) (x2 5) (y1 6) (y2 6)))
(assert (wall (x1 6) (x2 6) (y1 2) (y2 3)))

(assert (figure (x 3) (y 3) (type food)))
(assert (figure (x 4) (y 4) (type food)))
(assert (figure (x 2) (y 6) (type food)))
(assert (figure (x 2) (y 6) (type pac-man) (direction right)))
(assert (figure (x 1) (y 1) (type monster) (direction up)))