(deftemplate figure
    (slot x (allowed-numbers 1 2 3 4 5 6 7))
    (slot y (allowed-numbers 1 2 3 4 5 6 7))
    (slot type (allowed-values pac-man monster food))
    (slot direction (allowed-values up down left rigth none) (default none))
)
(deftemplate wall
    (slot x1 (allowed-numbers 0 1 2 3 4 5 6 7 8))
    (slot x2 (allowed-numbers 0 1 2 3 4 5 6 7 8))
    (slot y1 (allowed-numbers 0 1 2 3 4 5 6 7 8))
    (slot y2 (allowed-numbers 0 1 2 3 4 5 6 7 8))
)

;DEFINICION DE REGLAS
(defrule pac-man-wins-game
    (not (figure (type food)))
    (figure (type pac-man) (direction ~none))
    =>
    (printout t "Pac-man wins game" crlf)
)
(defrule pac-man-loses-game
    (not (figure (type pac-man)))
    (figure (type monster) (direction ~none))
    =>
    (printout t "Pac-man loses game" crlf)
)
(defrule finish-game
    (not (figure (type food)))
    ?figure <-(figure (type ~food) (direction ~none))
    =>
    (modify ?figure (direction none))
)
(defrule pac-man-eats-food
    ?food <- (figure (x ?x) (y ?y) (type food))
    (figure (x ?x) (y ?y) (type ~food))
    =>
    (retract ?food)
    (printout t "Pac-man eats food" crlf)
)
(defrule monster-kills-pac-man
    ?pac-man <- (figure (x ?x) (y ?y) (type pac-man))
    (figure (x ?x) (y ?y) (type monster))
    =>
    (retract ?pac-man)
    (printout t "Monster killed Pac-man" crlf)
)

(defrule pac-man-turn-to-up
    (not (wall (x1 ?x) (x2 ?x) (y1 ?y) (y2 ?y)))
    (figure (x ?x) (y ?y) (type pac-man) (direction up))
    =>
    (printout t "Pac-man move to up" crlf)
)
(defrule pac-man-turn-to-down
    (not (wall (x1 ?x) (x2 ?x) (y1 ?y) (y2 ?y)))
    (figure (x ?x) (y ?y) (type pac-man) (direction down))
    =>
    (printout t "Pac-man move to down" crlf)
)
(defrule pac-man-turn-to-left
    (not (wall (x1 ?x) (x2 ?x) (y1 ?y) (y2 ?y)))
    (figure (y ?y) (x ?x) (type pac-man) (direction left))
    =>
    (printout t "Pac-man move to left" crlf)
)
(defrule pac-man-turn-to-rigth
    (not (wall (x1 ?x) (x2 ?x) (y1 ?y) (y2 ?y)))
    (figure (x ?x) (y ?y) (type pac-man) (direction rigth))
    =>
    (printout t "Pac-man move to rigth" crlf)
    (assert (figure (x ?x) (y ?y) (type pac-man) (direction up)))
)

(defrule change-direction-to-moster
    ?moster <- (figure (type monster) (direction ?direction))
    =>
    (switch (mod (random 0 100) 4)
    (case 0 then (modify ?moster (direction up)))
    (case 1 then (modify ?moster (direction down)))
    (case 2 then (modify ?moster (direction left)))
    (case 3 then (modify ?moster (direction rigth)))
    )
)
;DEFINICION DE HECHOS
(assert (wall (x1 0) (x2 0) (y1 0) (y2 8)))
(assert (wall (x1 8) (x2 8) (y1 0) (y2 8)))
(assert (wall (x1 0) (x2 8) (y1 0) (y2 0)))
(assert (wall (x1 0) (x2 8) (y1 8) (y2 8)))

(assert (wall (x1 2) (x2 4) (y1 2) (y2 2)))
(assert (wall (x1 1) (x2 5) (y1 7) (y2 7)))
(assert (wall (x1 6) (x2 6) (y1 2) (y2 3)))

(assert (figure (x 3) (y 3) (type food)))
(assert (figure (x 4) (y 4) (type food)))
(assert (figure (x 2) (y 6) (type food)))
(assert (figure (x 6) (y 4) (type pac-man) (direction rigth)))

(assert (figure (x 1) (y 5) (type monster) (direction up)))