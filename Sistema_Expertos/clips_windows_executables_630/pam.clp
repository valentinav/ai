(deftemplate figure
  (slot x (allowed-numbers 0 1 2 3 4 5 6 7))
  (slot y (allowed-numbers 0 1 2 3 4 5 6 7))
  (slot type (allowed-values pac-man monster food))
  (slot direction (allowed-values up down left right none) (default none))
)
(deftemplate wall
  (slot x1 (allowed-numbers 0 1 2 3 4 5 6 7))
  (slot x2 (allowed-numbers 0 1 2 3 4 5 6 7))
  (slot y1 (allowed-numbers 0 1 2 3 4 5 6 7))
  (slot y2 (allowed-numbers 0 1 2 3 4 5 6 7))
)

(defrule figure-moves-to-up
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type pac-man) (direction up))
  (test (> ?y 0))
  (test (eq ?y1 ?y2))
  (test (or (neq ?y1 :(- ?y 1)) (> ?x1 ?x) (< ?x2 ?x)))
  =>
  (modify ?figure (y (- ?y 1)))
  (printout t "Up pacman " ?x "," ?y " => " ?x "," (- ?y 1)  crlf)
)
(defrule figure-moves-to-down
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type pac-man) (direction down))
  (test (< ?y 7))
  (test (eq ?y1 ?y2))
  (test (or (neq ?y1 :(+ ?y 1)) (> ?x1 ?x) (< ?x2 ?x)))
  =>
  (modify ?figure (y (+ ?y 1)))
  (printout t "Down pacman " ?x "," ?y " => " ?x "," (+ ?y 1)  crlf)
)
(defrule figure-moves-to-left
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type pac-man) (direction left))
  (test (> ?x 0))
  (test (eq ?x1 ?x2))
  (test (or (neq ?x1 :(- ?x 1)) (> ?y1 ?y) (< ?y2 ?y)))
  =>
  (modify ?figure (x (- ?x 1)))
  (printout t "left pacman " ?x "," ?y " => " (- ?x 1) "," ?y  crlf)
)
(defrule figure-moves-to-right
  (wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
  ?figure <-(figure (x ?x) (y ?y) (type pac-man) (direction right))
  (test (< ?x 7))
  (test (eq ?x1 ?x2))
  (test (or (neq ?x1 :(+ ?x 1)) (> ?y1 ?y) (< ?y2 ?y)))
  =>
  (modify ?figure (x (+ ?x 1)))
  (printout t "right pacman " ?x "," ?y " => " (+ ?x 1) "," ?y   crlf)
)
(defrule change-direction-to-pacman
  ?pacman <- (figure (type pac-man) (direction ?direction))
  =>
  (switch (mod (random 0 100) 4)
    (case 0 then
      (modify ?pacman (direction up))
      (printout t "pacman direction up" crlf)
    )
    (case 1 then
      (modify ?pacman (direction down))
      (printout t "pacman direction down" crlf)
    )
    (case 2 then
      (modify ?pacman (direction left))
      (printout t "pacman direction left" crlf)
    )
    (case 3 then
      (modify ?pacman (direction right))
      (printout t "pacman direction right" crlf)
    )
  )
)

(assert (figure (x 2) (y 6) (type pac-man) (direction up)))
(assert (wall (x1 2) (x2 6) (y1 3) (y2 3)))
(assert (wall (x1 4) (x2 4) (y1 5) (y2 7)))

    ;    0  1  2  3  4  5  6  7
    ; 0  |  |  |  |  |  |  |  |
    ; 1  |        |           |
    ; 2  |  |  |  |     |     |
    ; 3  |  |  |  |  |  |     |
    ; 4  |        |  .  |  .  |
    ; 5  |  *  |  |  |  |  #  |
    ; 6  |              |  .  |
    ; 7  |  |  |  |  |  |  |  |
